package com.aowdriver.wraper_layer.retrofit;


import com.aowdriver.intigration_layer.model.request_model.device_data_access_model;
import com.google.gson.JsonObject;
;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Anand on 03//17.
 */

public interface IRetrofitAPIMethods {


    //Admins getUserName Suggestions
    @GET("getAddLiveLink")
    Call<JsonObject> getAds();

    @Headers({"Content-Type: application/json"})
    @GET("slider/all")
    Call<JsonObject> getSlider(@Query("X-API-KEY") String key, @Query("limit") String limit, @Query("offset") String offset);



    @GET("getBottomsBanner")
    Call<JsonObject> getBanners();

    // @GET("track/alltrackbystyle?id={id}")
    @Headers({"Content-Type: application/json"})
    @GET("track/alltrackbystyle")
    Call<JsonObject> alltrackbystyle(@Query("id") String id, @Query("X-API-KEY") String key);

    // http://52.8.218.210/av_application/endpoint/appusers/login

//    @GET("users/getAllChoiceNo")
//    Call<JsonObject> getAllChoiceNumbers();
//
//    //@FormUrlEncoded getRetailerDATA


    @Headers({"Content-Type: application/json"})
    @POST("appusers/login")
    Call<JsonObject> userLogin(@Body device_data_access_model getAllTimeDAO);



}
