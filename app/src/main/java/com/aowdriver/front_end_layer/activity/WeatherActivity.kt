package com.aowdriver.front_end_layer.activity

import android.content.ContentValues.TAG
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.aowdriver.R
import kotlinx.android.synthetic.main.activity_weather.*
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*
import org.json.JSONObject
import org.eclipse.paho.client.mqttv3.MqttCallback


class WeatherActivity : AppCompatActivity() {
    var isConnected: Boolean = false
    private var isOn: Boolean = false
    var client: MqttAndroidClient? = null

    var mqttCallback: MqttCallback? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_weather)
        initView()
        onclick()

        callMqTT()

    }


    fun initView() {


    }

    fun onclick() {

        imgBack.setOnClickListener { finish() }

    }

//
//    override fun connectionLost(cause: Throwable?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun deliveryComplete(token: IMqttDeliveryToken?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun messageArrived(topic: String?, message: MqttMessage?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//
//
//        Toast.makeText(this, "Topic: $topic\nMessage: $message", Toast.LENGTH_LONG).show()
//
//        val messageString: String = message.toString()
//        val json = JSONObject(messageString)
//
//        if (json.has("temperature")) {
//            val temp = json.get("temperature")
//            temptxt.text = temp.toString();
//
//        }
//        if (json.has("humidity")) {
//            val humidity = json.get("humidity")
//            humiditytxt.text = humidity.toString();
//
//        }
//        if (json.has("light")) {
//            val light = json.get("light")
//            lighttxt.text = light.toString();
//
//        }
//        if (json.has("moisture")) {
//            val moisture = json.get("moisture")
//            moisturetxt.text = moisture.toString();
//
//        }
//        if (json.has("ph")) {
//            val ph = json.get("ph")
//            phtxt.text = ph.toString();
//
//        }
//
//        if (json.has("rain")) {
//            val rain = json.get("rain")
//            raintxt.text = rain.toString();
//
//        }
//        if (json.has("motor")) {
//            val motor = json.get("motor")
//            if (motor.equals("1")) {
//                isOn = true
//                publishonoff?.text = "ON"
//
//            } else if (motor.equals("0")) {
//                isOn = false
//                publishonoff?.text = "OFF"
//
//            }
//
//        }
//    }

    fun callMqTT() {

        //MQTTConnect options : setting version to MQTT 3.1.1
        val options = MqttConnectOptions()
        options.mqttVersion = MqttConnectOptions.MQTT_VERSION_3_1
        options.userName = "bqt"
        options.password = "bqt@123".toCharArray()

        //Below code binds MainActivity to Paho Android Service via provided MqttAndroidClient
        // client interface
        //Todo : Check why it wasn't connecting to test.mosquitto.org. Isn't that a public broker.
        //Todo : .check why client.subscribe was throwing NullPointerException  even on doing subToken.waitForCompletion()  for Async                  connection estabishment. and why it worked on subscribing from within client.connect’s onSuccess(). SO
      // val clientId = MqttClient.generateClientId()
        client = MqttAndroidClient(WeatherActivity@ this, "tcp://52.8.144.182:1883",
                "1234")




        try {
            val token = client!!.connect(options)
            token.actionCallback = object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    // We are connected
                    isConnected = true

                    val toast = Toast.makeText(applicationContext, "Connection successful", Toast.LENGTH_SHORT).show()

                    //Subscribing to a topic door/status on broker.hivemq.com


                    // test code for connection

                    client!!.setCallback(object : MqttCallback {


                        override fun connectionLost(cause: Throwable) {
                            Log.d(TAG, "The Connection was lost.")
                        }

                        @Throws(Exception::class)
                        override fun messageArrived(topic: String, message: MqttMessage) {
                            Log.d(TAG, "Incoming message from $topic: " + message.toString())
                            //Toast.makeText(this, "Topic: $topic\nMessage: $message", Toast.LENGTH_LONG).show()
                           //Da val toast = Toast.makeText(applicationContext, "Topic: $topic\nMessage: $message", Toast.LENGTH_LONG).show()


                            val messageString: String = message.toString()
                            val json = JSONObject(messageString)

                            if (json.has("temperature")) {
                                val temp = json.get("temperature")
//                                temptxt.text = temp.toString();
                                temptxt.setText(""+ temp+ " lin °C")

                            }
                            if (json.has("humidity")) {
                                val humidity = json.get("humidity")
//                                humiditytxt.text = humidity.toString();
                                humiditytxt.setText(""+ humidity+ " %")

                            }
                            if (json.has("light")) {
                                val light = json.get("light")
                                lighttxt.text = light.toString();

                            }
                            if (json.has("moisture")) {
                                val moisture = json.get("moisture")
                                moisturetxt.text = moisture.toString();

                            }
                            if (json.has("ph")) {
                                val ph = json.get("ph")
                                phtxt.text = ph.toString();

                            }

                            if (json.has("rain")) {
                                val rain = json.get("rain")
                                raintxt.text = rain.toString();

                            }

                        }

                        override fun deliveryComplete(token: IMqttDeliveryToken) {

                        }
                    })

//                    end
                    val topic = "bqt/sensorData" //topic_at_which_you_want_app_to_subscribe
//                    val topic = "\$neucrack/gprs" //topic_at_which_you_want_app_to_subscribe
                    val qos = 1
                    try {
                        val subToken = client!!.subscribe(topic, qos)
                        subToken.setActionCallback(object : IMqttActionListener {
                            override fun onSuccess(asyncActionToken: IMqttToken) {
                                // successfully subscribed
                                val toast = Toast.makeText(applicationContext, "Successfully subscribed to: $topic", Toast.LENGTH_SHORT).show()

                            }

                            override fun onFailure(asyncActionToken: IMqttToken,
                                                   exception: Throwable) {
                                // The subscription could not be performed, maybe the user was not
                                // authorized to subscribe on the specified topic e.g. using wildcards
                                //Toast.makeText(WeatherActivity@this, "Couldn't subscribe to: $topic").show()

                                val toast = Toast.makeText(applicationContext, "Couldn't subscribe to: $topic", Toast.LENGTH_LONG)
                                toast.show()

                            }
                        })
                    } catch (e: MqttException) {
                        e.printStackTrace()
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }

                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    // Something went wrong e.g. connection timeout or firewall problems


                    val toast = Toast.makeText(applicationContext, "Connection failed", Toast.LENGTH_SHORT).show()

                }
            }
        } catch (e: MqttException) {
            e.printStackTrace()
        }


    }
}

//fun connect(topics: Array<String>? = null,
//            messageCallBack: ((topic: String, message: MqttMessage) -> Unit)? = null) {
//    try {
//
//
//        //client.connect()
//        client.setCallback(object : MqttCallback {
//            fun connectComplete(reconnect: Boolean, serverURI: String) {
//                topics?.forEach {
//
//                }
//                Log.d(TAG, "Connected to: $serverURI")
//            }
//
//            override fun connectionLost(cause: Throwable) {
//                Log.d(TAG, "The Connection was lost.")
//            }
//
//            @Throws(Exception::class)
//            override fun messageArrived(topic: String, message: MqttMessage) {
//                Log.d(TAG, "Incoming message from $topic: " + message.toString())
//                messageCallBack?.invoke(topic, message)
//            }
//
//            override fun deliveryComplete(token: IMqttDeliveryToken) {
//
//            }
//        })
//
//
//    } catch (e: MqttException) {
//        e.printStackTrace()
//    }




