package com.aowdriver.front_end_layer.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import com.aowdriver.R

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {


        if (v != null) {
            when (v.id) {


            }

        }

        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var mButLog: Button? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions,0)

        val permissionss = arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions,0)

        initView()
        onClick()

    }

    fun initView() {

        mButLog = findViewById(R.id.butLogin);
    }

    fun onClick() {

        // Another way to set button click listener
        mButLog!!.setOnClickListener {


            val intent = Intent(applicationContext, DashboardActivity::class.java)
            startActivity(intent)
            finish()


        }
    }
}