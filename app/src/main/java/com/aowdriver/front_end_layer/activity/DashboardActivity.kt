package com.aowdriver.front_end_layer.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.aowdriver.R
import com.aowdriver.front_end_layer.view.adapter.NavigationAdapter
import com.aowdriver.front_end_layer.view.fragment.*
import com.aowdriver.front_end_layer.view.fragment.FragmentHome.*
import com.aowdriver.intigration_layer.uitlity.MyApplication.LatitudeC
import com.aowdriver.intigration_layer.uitlity.MyApplication.longitudeC
import com.aowdriver.wraper_layer.retrofit.Config.DEVICE_MOBILE_NO
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.json.JSONObject
import java.util.*

class DashboardActivity : AppCompatActivity(), MqttCallback {

    public var AppContext: Context? = null

    public var DashboardActivityContext: DashboardActivity? = null
    private var toolbarLayout: View? = null
    private var mToolbar: Toolbar? = null
    private var toggle: ActionBarDrawerToggle? = null
    private var navigationView: NavigationView? = null
    private var adapter: ViewPagerAdapter? = null
    private var mListNav: RecyclerView? = null
    private var navgiationDrwArrayList: ArrayList<String>? = null

    public var latitude: String? = null
    public var longitude: String? = null


    private var tabLayout: TabLayout? = null
    private var viewPager: ViewPager? = null
    private var tabOne: TextView? = null
    private var tabTwo: TextView? = null
    private var tabThree: TextView? = null
    private var tabFive: TextView? = null
    private var tabFour: TextView? = null

    private var publishonoff: Button? = null
    private var isOn: Boolean = false



    internal lateinit var drawer: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AppContext = this
        DashboardActivityContext= DashboardActivity();

       // val mapFragment = supportFragmentManager
        initView()
        onClick()
        bottomBar()

        publishonoff?.setOnClickListener {

            // Toast.makeText(this, "Topic: On OFFF Button IS Clicked", Toast.LENGTH_LONG).show()
            if (isConnected) {
                val message: MqttMessage

                if (isOn) {
                    message = MqttMessage("{motor:0}".toByteArray())
                } else {
                    message = MqttMessage("{motor:1}".toByteArray())

                }
                message.qos = 2
                message.isRetained = false
                try {
                    client.publish("bqt/cmd/motor", message)
                    Toast.makeText(this, "Published The Message ", Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }


            } else {
                Toast.makeText(this, "Not Connected", Toast.LENGTH_SHORT).show()

            }
        }

    }

    private fun initView() {


        navgiationDrwArrayList = ArrayList()

        // navgiationDrwArrayList!!.add("Settings")
        navgiationDrwArrayList!!.add("About us")
        navgiationDrwArrayList!!.add("Contact us")
        navgiationDrwArrayList!!.add("How to use?")
        // navgiationDrwArrayList!!.add("Share app")
        navgiationDrwArrayList!!.add("Logout")
        toolbarLayout = findViewById(R.id.toolbarLayout)
        navigationView = findViewById<View>(R.id.nav_view) as NavigationView

        publishonoff = findViewById(R.id.publishonoff) as Button


        mListNav = navigationView!!.findViewById<View>(R.id.navlist) as RecyclerView
        mToolbar = toolbarLayout!!.findViewById(R.id.toolbar)
        mToolbar = findViewById<View>(R.id.toolbar) as Toolbar


        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout


        val mListInstallmentMangaer = LinearLayoutManager(this@DashboardActivity, LinearLayoutManager.VERTICAL, false)
        mListNav!!.layoutManager = mListInstallmentMangaer


        val navigationAdapter = NavigationAdapter(this, navgiationDrwArrayList)
        mListNav!!.adapter = navigationAdapter





        toggle = ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle!!.syncState()


    }

    private fun onClick() {


    }

    private fun bottomBar() {

        tabOne = LayoutInflater.from(this).inflate(R.layout.custom_tab, null) as TextView
        tabTwo = LayoutInflater.from(this).inflate(R.layout.custom_tab, null) as TextView
        tabThree = LayoutInflater.from(this).inflate(R.layout.custom_tab, null) as TextView
//        tabFive = LayoutInflater.from(this).inflate(R.layout.custom_tab, null) as TextView
//        tabFour = LayoutInflater.from(this).inflate(R.layout.custom_tab, null) as TextView

        viewPager = findViewById<View>(R.id.viewpager) as ViewPager
        tabLayout = findViewById<View>(R.id.tabs) as TabLayout
        setupViewPager(viewPager!!)

        tabLayout!!.setupWithViewPager(viewPager)

        setupTabIcons()


        tabLayout!!.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                //                tab.getIcon().setColorFilter(Color.parseColor("#e22127"), PorterDuff.Mode.SRC_IN);
                if (tab.position == 0) {
                    //   tabLayout.getTabAt(0).setIcon(R.drawable.home_red);


                    tabOne!!.setTextColor(resources.getColor(R.color.colorPrimary))
                    tabTwo!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
                    tabThree!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabFour!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabFive!!.setTextColor(resources.getColor(R.color.secondaryTextColor))


                    tabOne!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.active_home, 0, 0)
                    tabTwo!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_add, 0, 0)
                    tabThree!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.location, 0, 0)
//                    tabFour!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_loan, 0, 0)
//                    tabFive!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_profile, 0, 0)


                } else if (tab.position == 1) {


                    //                    mToolbarName.setText("Discover");

                    tabOne!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
                    tabTwo!!.setTextColor(resources.getColor(R.color.colorPrimary))
                    tabThree!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabFour!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabFive!!.setTextColor(resources.getColor(R.color.secondaryTextColor))


                    tabOne!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_home, 0, 0)
                    tabTwo!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.active_add, 0, 0)
                    tabThree!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.location, 0, 0)
//                    tabFour!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_loan, 0, 0)
//                    tabFive!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_profile, 0, 0)


                } else if (tab.position == 2) {


                    tabOne!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
                    tabTwo!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
                    tabThree!!.setTextColor(resources.getColor(R.color.colorPrimary))
//                    tabFour!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabFive!!.setTextColor(resources.getColor(R.color.secondaryTextColor))

                    tabOne!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_home, 0, 0)
                    tabTwo!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_add, 0, 0)
                    tabThree!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.location, 0, 0)
//                    tabFour!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_loan, 0, 0)
//                    tabFive!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_profile, 0, 0)


                }
//
//
//                else if (tab.position == 3) {
//
//
//                    tabOne!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabTwo!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabThree!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabFour!!.setTextColor(resources.getColor(R.color.colorPrimary))
//                    tabFive!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//
//
//                    tabOne!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_home, 0, 0)
//                    tabTwo!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_add, 0, 0)
//                    tabThree!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_earning, 0, 0)
//                    tabFour!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.active_loan, 0, 0)
//                    tabFive!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_profile, 0, 0)
//
//
//                } else if (tab.position == 4) {
//
//                    tabOne!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabTwo!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabThree!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabFour!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
//                    tabFive!!.setTextColor(resources.getColor(R.color.colorPrimary))
//
//                    //
//                    tabOne!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_home, 0, 0)
//                    tabTwo!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_add, 0, 0)
//                    tabThree!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_earning, 0, 0)
//                    tabFour!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_loan, 0, 0)
//                    tabFive!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.active_profile, 0, 0)
//
//
//                }


            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

                //((AppCompatActivity) MusicPlayerBaseActivity.this).getSupportActionBar().setTitle("Music");

                //                tab.getIcon().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
                //                tab.getIcon().setColorFilter(Color.parseColor("#767676"), PorterDuff.Mode.SRC_IN);
                tabOne!!.setTextColor(resources.getColor(R.color.secondaryTextColor))
            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

    private fun setupViewPager(viewPager: ViewPager) {
        adapter = ViewPagerAdapter(supportFragmentManager)
        adapter!!.addFragment(FragmentHome(), "Home")
        adapter!!.addFragment(FragmentVehicle(), "Add")
        adapter!!.addFragment(FragmentMap(), "Map")
//        adapter!!.addFragment(FragmentLoan(), "Loan")
//        adapter!!.addFragment(FragmentProfile(), "Profile")
        //        adapter.addFragment(new FragmentFriendRequest(), "Request");

        viewPager.offscreenPageLimit = 12
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }

    }


    //    private void selectedTAB() {
    //
    //
    //
    //
    //
    //
    //        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
    //            @Override
    //            public void onTabSelected(TabLayout.Tab tab) {
    //                int position = tab.getPosition();
    //
    ////                int position = tab.getPosition();
    //                int pos = viewPager.getCurrentItem();
    ////                Fragment fragment = (Fragment) adapter.getPageTitle(pos);
    //
    ////                if(position==0)
    ////                {
    ////                    Toast.makeText(DashboardActivity.this, "called", Toast.LENGTH_SHORT).show();
    ////                    FragmentHomePage articleFrag = (FragmentHomePage) getSupportFragmentManager().findFragmentByTag("Home");
    ////                    articleFrag.getWallPosts(mToken);
    ////                }
    //            }
    //
    //            @Override
    //            public void onTabUnselected(TabLayout.Tab tab) {
    //
    //
    //            }
    //
    //            @Override
    //            public void onTabReselected(TabLayout.Tab tab) {
    //
    //            }
    //        });
    //    }


    /// Set Up Tab Icons


    private fun setupTabIcons() {

        tabOne!!.text = "Home"
        tabOne!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.active_home, 0, 0)
        tabLayout!!.getTabAt(0)!!.customView = tabOne
        tabOne!!.setTextColor(resources.getColor(R.color.colorPrimary))

        tabTwo!!.text = "Add"
        tabTwo!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_add, 0, 0)
        tabLayout!!.getTabAt(1)!!.customView = tabTwo

        tabThree!!.text = "Map"
        tabThree!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.location, 0, 0)
        tabLayout!!.getTabAt(2)!!.customView = tabThree

//
//        tabFour!!.text = "Loan"
//        tabFour!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_loan, 0, 0)
//        tabLayout!!.getTabAt(3)!!.customView = tabFour
//
//        tabFive!!.text = "Profile"
//        tabFive!!.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.deactive_profile, 0, 0)
//        tabLayout!!.getTabAt(4)!!.customView = tabFive
    }


    override fun connectionLost(cause: Throwable) {
        isConnected = false;
    }

    @Throws(Exception::class)
    override fun messageArrived(topic: String, message: MqttMessage) {

//        Toast.makeText(this, "Topic: $topic\nMessage: $message", Toast.LENGTH_LONG).show()
//        Toast.makeText(this,"revised",Toast.LENGTH_SHORT).show()
//FragmentHome().getValues(message)

        val messageString: String = message.toString()
        val json = JSONObject(messageString)

        if (json.has("latitude")) {
            latitude = json.getString("latitude")
            LatitudeC= json.getString("latitude")
        }

        if (json.has("Latitude")) {
            latitude = json.getString("Latitude")
            LatitudeC= json.getString("Latitude")
        }


        if (json.has("Longitude")) {
            longitude = json.getString("Longitude")
            longitudeC= json.getString("Longitude")
        }

        if (json.has("Longitude")) {
            longitude = json.getString("Longitude")
            longitudeC= json.getString("longitude")
        }

        if (json.has("imea")) {
            val temp = json.get("imea") as String

            // Subcribe
            // call register deivce API and  subcribe bqt/device_status/"imeaNo" using Imea

            //DEVICE_MOBILE_NO =userInputDialogEditText.getText().toString();
            val uri = Uri.parse("smsto:" + DEVICE_MOBILE_NO)
            val intent = Intent(Intent.ACTION_SENDTO, uri)
            val UserMobileNo = "8669072307"
            intent.putExtra("sms_body", "#ACKBQTGPS*")
            startActivity(intent)

        }


            FragmentHome().getValues(json)

        if (json.has("temperature")) {
            val temp = json.get("temperature")
//            humiditytxt.text=temp.toString()
//            mTxtTempFrag.text=temp.toString()


        }
        if (json.has("humidity")) {
            val humidity = json.get("humidity")


        }
        if (json.has("light")) {
            val light = json.get("light")


        }
        if (json.has("moisture")) {
            val moisture = json.get("moisture")


        }
        if (json.has("ph")) {
            val ph = json.get("ph")


        }

        if (json.has("rain")) {
            val rain = json.get("rain")


        }
        if (json.has("motor")) {
            val motor = json.get("motor")
            if (motor.equals("1")) {
                isOn = true
                publishonoff?.text = "ON"

            } else if (motor.equals("0")) {
                isOn = false
                publishonoff?.text = "OFF"

            }

        }


    }

    override fun deliveryComplete(token: IMqttDeliveryToken) {

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            R.id.publishonoff -> {
                Toast.makeText(this, "Topic: On OFFF Button IS Clicked", Toast.LENGTH_LONG).show()

                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }


}

