package com.aowdriver.front_end_layer.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

import com.aowdriver.R
import com.aowdriver.intigration_layer.uitlity.DistanceCalculator
import com.aowdriver.intigration_layer.uitlity.MyApplication

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.MqttMessage
import kotlin.concurrent.fixedRateTimer


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, MqttCallback {


    private var isMapready: Boolean?= null


    override fun messageArrived(topic: String?, message: MqttMessage?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.




    }

    override fun connectionLost(cause: Throwable?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deliveryComplete(token: IMqttDeliveryToken?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var mMap: GoogleMap

    var isConnected: Boolean = false
    var client: MqttAndroidClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        //callMqtt();

        fixedRateTimer("timer",false,0,7000){
            this@MapsActivity.runOnUiThread {

                if(MyApplication.longitudeC !=null && mMap !=null )


                {
                   var LatLng1 = LatLng(18.5204, 73.8567);
                    var LatLng2 = LatLng(MyApplication.LatitudeC!!.toDouble(),MyApplication.longitudeC!!.toDouble())

                    var DistanceCalculator = DistanceCalculator().greatCircleInMeters(LatLng1, LatLng2)

                    Toast.makeText(applicationContext, "object is "+ DistanceCalculator+" mtr away from origin", Toast.LENGTH_LONG).show()


                    val pune = LatLng(MyApplication.LatitudeC!!.toDouble(), MyApplication.longitudeC!!.toDouble())
                    mMap.addMarker(MarkerOptions().position(pune).title("Marker in Sydney"))
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(pune))
                    val zoom = CameraUpdateFactory.zoomTo(17f)
                    mMap.animateCamera(zoom);
                }

            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        isMapready==true

        val pune = LatLng(18.5204, 73.8567)
        mMap.addMarker(MarkerOptions().position(pune).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pune))
        val zoom = CameraUpdateFactory.zoomTo(17f)
        mMap.animateCamera(zoom);
        //18.5204° N, 73.8567° E




    }






}
