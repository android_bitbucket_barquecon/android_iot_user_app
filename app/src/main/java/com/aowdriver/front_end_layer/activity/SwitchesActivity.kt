package com.aowdriver.front_end_layer.activity

import android.content.ContentValues
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.widget.Toast
import com.aowdriver.R
import com.aowdriver.front_end_layer.view.adapter.SwitchesGridAdapter
import com.aowdriver.intigration_layer.model.request_model.DeviceStatusModel
import com.aowdriver.intigration_layer.uitlity.Utility
import kotlinx.android.synthetic.main.activity_swicthes.*
import kotlinx.android.synthetic.main.activity_weather.*
import kotlinx.android.synthetic.main.toolbar_new.*
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*
import org.json.JSONObject

class SwitchesActivity : AppCompatActivity(), MqttCallback {

    var isConnected: Boolean = false
    private var isOn: Boolean = false
    var deviceStatusModel: DeviceStatusModel? = null
    var client: MqttAndroidClient? = null
    var switchesGridAdapter: SwitchesGridAdapter? = null
    var messageLast: MqttMessage? = null

    var deviceMac: String = "5C:CF:7F:78:0E:DA"
    lateinit var jsonSwitch: JSONObject

    override fun messageArrived(topic: String?, message: MqttMessage?) {

        if (topic != null)
            if (topic.contains("set_switch")) {

                if (topic.contains(deviceMac)) {
                    var messageStrings: String = message.toString()
                    jsonSwitch = JSONObject(messageStrings)
                    if (deviceStatusModel != null) {
                        deviceStatusModel!!.jsonSwitch = jsonSwitch
                    }
                    //refresh  Adapater
                    switchesGridAdapter!!.notifyDataSetChanged()


                }

            }
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun connectionLost(cause: Throwable?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deliveryComplete(token: IMqttDeliveryToken?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_swicthes)
        deviceStatusModel = DeviceStatusModel()
        initView()
        onClick()
        callMqTT()
    }

    fun initView() {

        Utility.sharedInstance.showProgressDialog(this)

        gridviewSw.setLayoutManager(GridLayoutManager(this, 2))

        txtToolbarTitle.setText("Switches")
        val imgStatus = intArrayOf(R.drawable.ic_power_off, R.drawable.ic_power_off, R.drawable.ic_power_off, R.drawable.ic_power_off, R.drawable.ic_power_off, R.drawable.ic_power_off, R.drawable.ic_power_off, R.drawable.ic_power_off)
        val txtStatus = arrayOf("OFF", "OFF ", "OFF", "OFF", "OFF", "OFF", "OFF", "OFF")
        val switches = arrayOf("Swicth-1", "Swicth-2 ", "Swicth-3", "Swicth-4", "Swicth-5", "Swicth-6", "Swicth-7", "Swicth-8")


        if (deviceStatusModel != null) {
            deviceStatusModel!!.deviceNAme = "xyz"
            deviceStatusModel!!.deviceid = "0"
        }

        switchesGridAdapter = SwitchesGridAdapter(
                this, txtStatus, imgStatus, deviceStatusModel,switches)
        gridviewSw.setAdapter(switchesGridAdapter)

        imgBackLayout.setOnClickListener { finish() }

        AllOFF.setOnClickListener {

            Utility.sharedInstance.showProgressDialog(this)


            var mqttM: MqttMessage = MqttMessage()
            var messges: String = "{\"D1\":\"0\",\"D2\":\"0\",\"D3\":\"0\",\"D6\":\"0\",\"D7\":\"0\",\"D8\":\"0\",\"D4\":\"0\",\"D5\":\"0\"}"
            mqttM.payload = messges.toString().toByteArray()
            // mqttM.payload=''
            client!!.publish("bqt/device_status/5C:CF:7F:78:0E:DA", mqttM)

        }

        AllON.setOnClickListener {

            Utility.sharedInstance.showProgressDialog(this)
            var mqttM: MqttMessage = MqttMessage()
            var messges: String = "{\"D1\":\"1\",\"D2\":\"1\",\"D3\":\"1\",\"D6\":\"1\",\"D7\":\"1\",\"D8\":\"1\",\"D4\":\"1\",\"D5\":\"1\"}"
            mqttM.payload = messges.toString().toByteArray()
            // mqttM.payload=''
            client!!.publish("bqt/device_status/5C:CF:7F:78:0E:DA", mqttM)
        }
    }

    fun onClick() {}


    fun callMqTT() {

        //MQTTConnect options : setting version to MQTT 3.1.1
        val options = MqttConnectOptions()
        options.mqttVersion = MqttConnectOptions.MQTT_VERSION_3_1
        options.userName = "bqt"
        options.password = "bqt@123".toCharArray()

        //Below code binds MainActivity to Paho Android Service via provided MqttAndroidClient
        // client interface
        //Todo : Check why it wasn't connecting to test.mosquitto.org. Isn't that a public broker.
        //Todo : .check why client.subscribe was throwing NullPointerException  even on doing subToken.waitForCompletion()  for Async                  connection estabishment. and why it worked on subscribing from within client.connect’s onSuccess(). SO
        // val clientId = MqttClient.generateClientId()
        client = MqttAndroidClient(WeatherActivity@ this, "tcp://52.8.144.182:1883",
                "1234")




        try {
            val token = client!!.connect(options)
            token.actionCallback = object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    // We are connected
                    isConnected = true

                    val toast = Toast.makeText(applicationContext, "Connection successful", Toast.LENGTH_SHORT).show()

                    //Subscribing to a topic door/status on broker.hivemq.com


                    // test code for connection

                    client!!.setCallback(object : MqttCallback {


                        override fun connectionLost(cause: Throwable) {
                            Log.d(ContentValues.TAG, "The Connection was lost.")
                        }

                        @Throws(Exception::class)
                        override fun messageArrived(topic: String, message: MqttMessage) {
                            Log.d(ContentValues.TAG, "Incoming message from $topic: " + message.toString())
                            //Toast.makeText(this, "Topic: $topic\nMessage: $message", Toast.LENGTH_LONG).show()
                            //Da val toast = Toast.makeText(applicationContext, "Topic: $topic\nMessage: $message", Toast.LENGTH_LONG).show()



                            if (topic.contains("set_switch")) {

                                if (messageLast != null) {
                                    messageLast = message

                                    if (topic.contains(deviceMac)) {
                                        var messageStrings: String = message.toString()
                                        jsonSwitch = JSONObject(messageStrings)
                                        if (deviceStatusModel != null) {
                                            deviceStatusModel!!.jsonSwitch = jsonSwitch
                                        }
                                        //refresh  Adapater
                                        switchesGridAdapter!!.notifyDataSetChanged()


                                    }
                                } else {

                                    if (messageLast != message) {

                                        messageLast = message

                                        if (topic.contains(deviceMac)) {
                                            var messageStrings: String = message.toString()
                                            jsonSwitch = JSONObject(messageStrings)
                                            if (deviceStatusModel != null) {
                                                deviceStatusModel!!.jsonSwitch = jsonSwitch
                                            }
                                            //refresh  Adapater
                                            switchesGridAdapter!!.notifyDataSetChanged()


                                        }
                                    }
                                }


                            }


                            val messageString: String = message.toString()
                            val json = JSONObject(messageString)

                            if (json.has("temperature")) {
                                val temp = json.get("temperature")
//                                temptxt.text = temp.toString();
                                temptxt.setText("" + temp + " lin °C")

                            }
                            if (json.has("humidity")) {
                                val humidity = json.get("humidity")
//                                humiditytxt.text = humidity.toString();
                                humiditytxt.setText("" + humidity + " %")

                            }
                            if (json.has("light")) {
                                val light = json.get("light")
                                lighttxt.text = light.toString();

                            }
                            if (json.has("moisture")) {
                                val moisture = json.get("moisture")
                                moisturetxt.text = moisture.toString();

                            }
                            if (json.has("ph")) {
                                val ph = json.get("ph")
                                phtxt.text = ph.toString();

                            }

                            if (json.has("rain")) {
                                val rain = json.get("rain")
                                raintxt.text = rain.toString();

                            }

                        }

                        override fun deliveryComplete(token: IMqttDeliveryToken) {

                        }
                    })

//                    end
                    val topic = "bqt/sensorData" //topic_at_which_you_want_app_to_subscribe
//                    val topic = "\$neucrack/gprs" //topic_at_which_you_want_app_to_subscribe
                    val topic3 = "bqt/set_switch/5C:CF:7F:78:0E:DA"
                    val qos = 1
                    try {
                        val subToken = client!!.subscribe(topic3, qos)
                        subToken.setActionCallback(object : IMqttActionListener {
                            override fun onSuccess(asyncActionToken: IMqttToken) {
                                // successfully subscribed
                                val toast = Toast.makeText(applicationContext, "Successfully subscribed to: $topic", Toast.LENGTH_SHORT).show()

                            }

                            override fun onFailure(asyncActionToken: IMqttToken,
                                                   exception: Throwable) {
                                // The subscription could not be performed, maybe the user was not
                                // authorized to subscribe on the specified topic e.g. using wildcards
                                //Toast.makeText(WeatherActivity@this, "Couldn't subscribe to: $topic").show()

                                val toast = Toast.makeText(applicationContext, "Couldn't subscribe to: $topic", Toast.LENGTH_LONG)
                                toast.show()

                            }
                        })
                    } catch (e: MqttException) {
                        e.printStackTrace()
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }

                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    // Something went wrong e.g. connection timeout or firewall problems


                    val toast = Toast.makeText(applicationContext, "Connection failed", Toast.LENGTH_SHORT).show()

                }
            }
        } catch (e: MqttException) {
            e.printStackTrace()
        }


    }


    fun switcOn(pos: Int) {

        Utility.sharedInstance.showProgressDialog(this)


        try {
            var kk: Int = pos + 1
            var key: String = "D" + kk

            var value = deviceStatusModel!!.jsonSwitch.getString(key)

            if (value == "1") {

                deviceStatusModel!!.jsonSwitch.put(key, "0")


            } else {
                deviceStatusModel!!.jsonSwitch.put(key, "1")

            }


            var newMsg: String = deviceStatusModel!!.jsonSwitch.toString()

            var mqttM: MqttMessage = MqttMessage()
            var messges: String = "{\"D1\":\"1\",\"D2\":\"1\",\"D3\":\"1\",\"D6\":\"1\",\"D7\":\"1\",\"D8\":\"1\",\"D4\":\"1\",\"D5\":\"1\"}"
            mqttM.payload = newMsg.toString().toByteArray()
            // mqttM.payload=''
            client!!.publish("bqt/device_status/5C:CF:7F:78:0E:DA", mqttM)


        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    fun removeProgress()
    {
        Utility.sharedInstance.dismissProgressDialog()
    }
}