package com.aowdriver.front_end_layer.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aowdriver.R;
import com.aowdriver.front_end_layer.activity.SwitchesActivity;
import com.aowdriver.intigration_layer.model.request_model.DeviceStatusModel;
import com.aowdriver.intigration_layer.uitlity.Utility;

import org.json.JSONException;

public class SwitchesGridAdapter extends RecyclerView.Adapter<SwitchesGridAdapter.MyViewHolder> {
    private Context mContext;
    private int img[];

    private DeviceStatusModel deviceStatusModel;
    private String name[];
    private String[] switches;
    LayoutInflater inflter;


    public SwitchesGridAdapter(Context activity, String[] name, int[] img,DeviceStatusModel deviceStatusModel,String[] swicthes) {
        this.img = img;
        this.name = name;
        this.mContext = activity;
        this.deviceStatusModel=deviceStatusModel;
        this.switches=swicthes;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,mTxtSwitches;
        ImageView mImg;
        private CardView mCardView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txtDeviceName);
            mTxtSwitches = (TextView) view.findViewById(R.id.txtSwitch);
            mCardView = (CardView) view.findViewById(R.id.cardView);
            mImg = (ImageView) view.findViewById(R.id.imgDevice);


        }
    }


    @Override
    public SwitchesGridAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_switch, parent, false);

        return new SwitchesGridAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SwitchesGridAdapter.MyViewHolder holder, final int position) {

        holder.mImg.setImageResource(img[position]);
        holder.title.setText(name[position]);
        holder.mTxtSwitches.setText(switches[position]);
         final int pos=position+1;

        String key="D"+pos;
        try {
        if(deviceStatusModel.jsonSwitch!=null)
            if (deviceStatusModel.jsonSwitch.has(key)){



                    String value=deviceStatusModel.jsonSwitch.getString(key);
                    if(value.equals("1")) {
                        holder.mImg.setImageResource(R.drawable.ic_power_on);
                        holder.title.setText("ON");
                        holder.title.setTextColor(mContext.getResources().getColor(R.color.black));
                        ((SwitchesActivity)mContext).removeProgress();
                    }
                    else
                    {

                        holder.mImg.setImageResource(R.drawable.ic_power_off);
                        holder.title.setText("OFF");
                        holder.title.setTextColor(mContext.getResources().getColor(R.color.light_bg_dark_disabled_text));
                        ((SwitchesActivity)mContext).removeProgress();
                    }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((SwitchesActivity)mContext).switcOn(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return img.length;
    }
}