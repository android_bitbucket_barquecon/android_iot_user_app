package com.aowdriver.front_end_layer.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aowdriver.R;
import com.aowdriver.front_end_layer.activity.SwitchesActivity;

public class DevicesGridAdapter extends RecyclerView.Adapter<DevicesGridAdapter.MyViewHolder> {
    private Context mContext;
    private int img[];

    private String name[];
    LayoutInflater inflter;


    public DevicesGridAdapter(Context activity, String[] name, int[] img) {
        this.img = img;
        this.name = name;
        this.mContext = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        ImageView mImg;
        private CardView mCardView;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txtDeviceName);
            mCardView = (CardView) view.findViewById(R.id.cardView);
            mImg = (ImageView) view.findViewById(R.id.imgDevice);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_devices, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.mImg.setImageResource(img[position]);
        holder.title.setText(name[position]);



        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SwitchesActivity.class);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return img.length;
    }
}