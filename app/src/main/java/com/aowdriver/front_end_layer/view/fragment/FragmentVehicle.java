package com.aowdriver.front_end_layer.view.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aowdriver.R;
//import com.aowdriver.front_end_layer.activity.DashboardActivity.AppContext;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.WIFI_SERVICE;
import static com.aowdriver.wraper_layer.retrofit.Config.DEVICE_MOBILE_NO;

public class FragmentVehicle  extends Fragment {
    private WifiManager wifiMgr;
    public static List<String> listNetwork = new ArrayList<String>();
    public ArrayList<ScanResult> configure_wifi_list= new ArrayList<>();

    private View myView;
    WebView mWebview;
    Button reload,reload1,back;
    ProgressDialog progressDialog;
    private int pos=0;
    private RecyclerView device_list_all;
    LinearLayout deviceList,linerWebView;


    public FragmentVehicle() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the list_item_switch for this fragment
        myView = inflater.inflate(R.layout.fragment_vehicle, container, false);
        progressDialog = new ProgressDialog( getActivity());
        wifiMgr = (WifiManager) getContext().getApplicationContext().getSystemService(WIFI_SERVICE);
        device_list_all=(RecyclerView)myView.findViewById(R.id.device_list_all);



        int permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)){
                Toast.makeText(getContext(), "The permission to Scan WIFI", Toast.LENGTH_SHORT).show();
            }else{
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }else{
            Toast.makeText(getContext(), "Location permissions already granted", Toast.LENGTH_SHORT).show();
            startScanRe();
        }



        mWebview =  myView.findViewById(R.id.mWebview);

        deviceList=myView.findViewById(R.id.deviceList);
        linerWebView=myView.findViewById(R.id.linerWebView);

        reload=myView.findViewById(R.id.reload);
        reload1=myView.findViewById(R.id.reload1);
        back=myView.findViewById(R.id.back);
      //  mWebview.loadUrl("http://192.168.4.1");
      //  getActivity().setContentView(mWebview );
//        mWebview.setWebViewClient(new WebViewClient());
//        mWebview.getSettings().setJavaScriptEnabled(true);
//        mWebview.loadUrl("http://192.168.4.1");   //add http at front
//        mWebview.getSettings().setUseWideViewPort(true);
//        mWebview.getSettings().setLoadWithOverviewMode(true);

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebview.setWebViewClient(new WebViewClient());
                mWebview.getSettings().setJavaScriptEnabled(true);
                mWebview.loadUrl("http://192.168.4.1");   //add http at front
                mWebview.getSettings().setUseWideViewPort(true);
                mWebview.getSettings().setLoadWithOverviewMode(true);
                showDialog();
            }
        });

        reload1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listNetwork = new ArrayList<String>();
                configure_wifi_list= new ArrayList<>();

                startScanRe();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listNetwork = new ArrayList<String>();
                configure_wifi_list= new ArrayList<>();
                startScanRe();
                deviceList.setVisibility(View.VISIBLE);
                linerWebView.setVisibility(View.GONE);
            }
        });




//        initView(myView);
//        Toast.makeText(getActivity(), "OnCreate", Toast.LENGTH_SHORT).show();
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getContext());
        View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getContext());
        alertDialogBuilderUserInput.setView(mView);

        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        // ToDo get user input here
                        DEVICE_MOBILE_NO =userInputDialogEditText.getText().toString();
                        Uri uri = Uri.parse("smsto:"+userInputDialogEditText.getText().toString());
                        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                        String UserMobileNo = "8669072307";
                        intent.putExtra("sms_body", "#ACTBQTGPS*"+UserMobileNo+"#");
                        startActivity(intent);
                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
        return myView;

    }
    @Override
    public void onDestroyView() {
        try {


            if (getView() != null) {
                ViewGroup parent = (ViewGroup) getView().getParent();
                parent.removeAllViews();
            }
        }
        catch (Exception e)
        {

        }
        super.onDestroyView();
    }
    public void startScanRe() {
        LocationManager locationManager = (LocationManager)  getContext().getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getContext().registerReceiver(mWifiScanReceiverRe,
                    new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            wifiMgr.startScan();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.setMessage("Please wait, while searching devices");
            progressDialog.show();
        } else {
           // action=1;
            // location_from_setting = true;
            Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            this.startActivity(i);
        }

    }

    void showDialog()
    {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getContext());
        View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getContext());
        alertDialogBuilderUserInput.setView(mView);

        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        // ToDo get user input here
                        DEVICE_MOBILE_NO =userInputDialogEditText.getText().toString();
                        Uri uri = Uri.parse("smsto:"+userInputDialogEditText.getText().toString());
                        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                        String UserMobileNo = "8669072307";
                        intent.putExtra("sms_body", "#ACTBQTGPS*"+UserMobileNo+"#");
                        startActivity(intent);
                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }
    private BroadcastReceiver mWifiScanReceiverRe = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            listNetwork.clear();
            if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                List<ScanResult> list = wifiMgr.getScanResults();
              //  Print("Wifi :---- BSSID: " + list.size());

                for (int i = 0; i < list.size(); i++) {
                 //   Print("Wifi :---- BSSID: " + list.get(i).BSSID + "  SSID: " + list.get(i).SSID);
                    if (list.get(i).SSID.contains("BQT-")) {
                        if(configure_wifi_list.size()>0)
                        {
                            boolean isalready=false;
                            for(int j=0;j<configure_wifi_list.size();j++)
                            {

                                if(configure_wifi_list.get(j).SSID.contains(list.get(i).SSID))
                                {
                                    isalready=true;
                                    break;
                                }else
                                {
                                    isalready=false;
                                    //goldmedal_wifi_appActivity.configure_wifi_list.add(list.get(i));
                                }


                            }
                            if(isalready==false)
                                configure_wifi_list.add(list.get(i));


                        }
                        else
                        {
                            configure_wifi_list.add(list.get(i));
                        }
                    }
                    else
                    {    String ssidNetowrk = list.get(i).SSID.replace(" ","");
                        if (!(ssidNetowrk.equals("")))
                            listNetwork.add(list.get(i).SSID);
                    }
                }
                if (progressDialog != null && progressDialog.isShowing()) {
                    //is running
                    progressDialog.dismiss();
                }


                List<String> listNetworks = new ArrayList<String>();

                if(configure_wifi_list.size()>0) {
                  //  yesDevice();
                    for (int j = 0; j < configure_wifi_list.size(); j++) {
                        listNetworks.add(configure_wifi_list.get(j).BSSID);

                    }
                    // configure_wifi_list.get(0).



                    MoviesAdapterStep2  mAdapterStep2 = new MoviesAdapterStep2(listNetworks);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( getContext().getApplicationContext());
                    device_list_all.setLayoutManager(mLayoutManager);
                   device_list_all.setItemAnimator(new DefaultItemAnimator());
                    device_list_all.setAdapter(mAdapterStep2);
                }
                else {
                  //  noDevice();
                }

                getContext().unregisterReceiver(mWifiScanReceiverRe);

            }
        }
    };

    public class MoviesAdapterStep2 extends RecyclerView.Adapter<MoviesAdapterStep2.MyViewHolder> {

        private List<String> moviesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, year, genre;
            View view;

            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.title);
                this.view=view;

            }
        }


        public MoviesAdapterStep2(List<String> moviesList) {
            this.moviesList = moviesList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_list_row, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            String movie = moviesList.get(position);
            holder.title.setText(configure_wifi_list.get(position).SSID);
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // selectedSSID.setText("Your's selected WiFi: "+moviesList.get(position));
                   // SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.selectedDeviceSSID,
                         //   moviesList.get(position));
                    pos=position;

                   // selectedDevice.setText("Your's selected device: "+moviesList.get(position));
                    checkpermisionStep2();

                    // saveWifi_step2.setVisibility(View.VISIBLE);

                }
            });
//            holder.genre.setText(movie.getGenre());
//            holder.year.setText(movie.getYear());
        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }
    }

    public void checkpermisionStep2(){


        {
            connectWiFi(configure_wifi_list.get(pos));
        }

    }


    public void connectWiFi(ScanResult scanResult) {
        try {

            Log.v("Wifi", "Item clicked, SSID " + scanResult.SSID + " Security : " + scanResult.capabilities);
            String networkSSID = scanResult.SSID;
            String networkPass = "123456789";
            WifiConfiguration conf = new WifiConfiguration();
            conf.SSID = "\"" + networkSSID + "\"";   // Please note the quotes. String should contain ssid in quotes
            conf.status = WifiConfiguration.Status.ENABLED;
            conf.priority = 40;

            if (scanResult.capabilities.toUpperCase().contains("WEP")) {
                Log.v("Wifi", "Configuring WEP");
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);

                if (networkPass.matches("^[0-9a-fA-F]+$")) {
                    conf.wepKeys[0] = networkPass;
                } else {
                    conf.wepKeys[0] = "\"".concat(networkPass).concat("\"");
                }

                conf.wepTxKeyIndex = 0;

            } else if (scanResult.capabilities.toUpperCase().contains("WPA")) {
                Log.v("Wifi", "Configuring WPA");

                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                conf.preSharedKey = "\"" + networkPass + "\"";

            } else {
                Log.v("Wifi", "Configuring OPEN network");
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                conf.allowedAuthAlgorithms.clear();
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            }


            int networkId = wifiMgr.addNetwork(conf);

            Log.v("Wifi", "Add result " + networkId);

            List<WifiConfiguration> list = wifiMgr.getConfiguredNetworks();
            for (WifiConfiguration i : list) {
                if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                    Log.v("Wifi", "WifiConfiguration SSID " + i.SSID);

                    boolean isDisconnected = wifiMgr.disconnect();
                    Log.v("Wifi", "isDisconnected : " + isDisconnected);

                    boolean isEnabled = wifiMgr.enableNetwork(i.networkId, true);
                    Log.v("Wifi", "isEnabled : " + isEnabled);

                    boolean isReconnected = wifiMgr.reconnect();
                    Log.v("Wifi", "isReconnected : " + isReconnected);

                    break;
                }
            }

            progressDialog = new ProgressDialog(getContext());
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.setMessage("Please wait..");
            progressDialog.show();

            getContext().registerReceiver(mWifiConnectivity,
                    new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

            //goldmedal_wifi_appActivity.sendBroadcast();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private BroadcastReceiver mWifiConnectivity = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMan.getActiveNetworkInfo();
            if (netInfo != null)
                //Toast.makeText(goldmedal_wifi_appActivity, "Broadcast " + netInfo.getType(), Toast.LENGTH_SHORT).show();
                if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                   // Print("Have Wifi Connection" + netInfo);
                    if (netInfo.getState() == NetworkInfo.State.CONNECTED && (netInfo.getExtraInfo().contains("BQT-"))) {
                        getContext().unregisterReceiver(mWifiConnectivity);
                        setLogin();

                        //progressDialog.dismiss();

                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        //is running
                        if (progressDialog != null && progressDialog.isShowing()) {
                            //is running
                            progressDialog.dismiss();
                        }
                    }
                    //Print("Don't have Wifi Connection");
//                Message message = new Message();
//                handler.sendMessageDelayed(message, 2000);
//                Handler handler = new Handler(new Handler.Callback() {
//                    @Override
//                    public boolean handleMessage(Message msg) {
//                        setLogin();
//                        return false;
//                    }
//                });
                }
        }
    };

    private void setLogin() {

        progressDialog.dismiss();
        deviceList.setVisibility(View.GONE);
        linerWebView.setVisibility(View.VISIBLE);
        mWebview.setWebViewClient(new WebViewClient());
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.loadUrl("http://192.168.4.1");   //add http at front
        mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setLoadWithOverviewMode(true);


    }


}
