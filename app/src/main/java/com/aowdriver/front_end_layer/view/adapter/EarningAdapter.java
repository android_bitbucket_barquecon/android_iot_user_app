package com.aowdriver.front_end_layer.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aowdriver.R;

import java.util.ArrayList;

public class EarningAdapter extends RecyclerView.Adapter<EarningAdapter.ViewHolder>{

    Context context;
    private ArrayList<String> arrayList;

    public EarningAdapter(Context activity, ArrayList<String> listVehicle) {
        this.context=activity;
        this.arrayList=listVehicle;

    }


    @Override
    public EarningAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the list_item_switch file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_earning_months, parent, false);
        EarningAdapter.ViewHolder gvh = new EarningAdapter.ViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(EarningAdapter.ViewHolder holder, final int position) {

        holder.txtview.setText(arrayList.get(position).toString());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtview;
        public ViewHolder(View view) {
            super(view);

            txtview=view.findViewById(R.id.txtMonth);
        }
    }
}