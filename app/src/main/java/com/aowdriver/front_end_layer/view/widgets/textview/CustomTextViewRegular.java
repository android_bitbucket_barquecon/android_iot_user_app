package com.aowdriver.front_end_layer.view.widgets.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by monica on 12/15/2015.
 */
public class CustomTextViewRegular extends android.support.v7.widget.AppCompatTextView {

    public CustomTextViewRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextViewRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
//        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "OstrichSansRounded-Medium.otf");
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Raleway-Regular.ttf");
//        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Roboto-Thin.ttf");
       // Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "sofiapro-light.otf");

        setTypeface(tf ,1);

    }

}
