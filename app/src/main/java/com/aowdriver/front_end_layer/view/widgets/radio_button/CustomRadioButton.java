package com.aowdriver.front_end_layer.view.widgets.radio_button;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by PrathaM on 4/25/2017.
 */

public class CustomRadioButton extends RadioButton {

    public CustomRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRadioButton(Context context) {
        super(context);
        init();
    }

    public void init() {
        //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "OstrichSansRounded-Medium.otf");
//        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Roboto-Thin.ttf");
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Raleway-Regular.ttf");
        //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "sofiapro-light.otf");
        setTypeface(tf ,1);

    }
}
