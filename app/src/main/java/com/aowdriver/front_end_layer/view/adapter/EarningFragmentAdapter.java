package com.aowdriver.front_end_layer.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aowdriver.R;

import java.util.ArrayList;

public class EarningFragmentAdapter extends RecyclerView.Adapter<EarningFragmentAdapter.ViewHolder>{

    Context context;
    private ArrayList<String> arrayList;

    public EarningFragmentAdapter(Context activity, ArrayList<String> listVehicle) {
        this.context=activity;
        this.arrayList=listVehicle;

    }


    @Override
    public EarningFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the list_item_switch file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_earning_fragment, parent, false);
        EarningFragmentAdapter.ViewHolder gvh = new EarningFragmentAdapter.ViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(EarningFragmentAdapter.ViewHolder holder, final int position) {

        holder.txtview.setText(arrayList.get(position).toString());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtview;
        public ViewHolder(View view) {
            super(view);

            txtview=view.findViewById(R.id.txtMonth);
        }
    }
}