package com.aowdriver.front_end_layer.view.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton

import com.aowdriver.R
import com.aowdriver.front_end_layer.activity.MapsActivity
import com.aowdriver.intigration_layer.uitlity.MyApplication.applicationContext
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import java.util.ArrayList


class FragmentMap : Fragment(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))


        val pune = LatLng(18.5204, 73.8567)
        mMap.addMarker(MarkerOptions().position(pune).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pune))
        val zoom = CameraUpdateFactory.zoomTo(17f)
        mMap.animateCamera(zoom);
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var myView: View? = null
    private var locations: ImageButton? = null
    private val mListEarningMonth: RecyclerView? = null



    private val listMonth = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the list_item_switch for this fragment
        myView = inflater.inflate(R.layout.fragment_map, container, false)

       // location
        initView(myView)



//       val mapFragment = myView.supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
//       mapFragment.getMapAsync(applicationContext)
//        initView(myView)
        //        Toast.makeText(getActivity(), "OnCreate", Toast.LENGTH_SHORT).show();
        return myView

    }

    private fun initView(myView: View?) {
        if (myView != null) {
            locations = myView.findViewById(R.id.location) as ImageButton
            locations!!.setOnClickListener {
                val intent = Intent (applicationContext, MapsActivity::class.java)
                startActivity(intent)

            }


        }

    }


}// Required empty public constructor

private fun SupportMapFragment.getMapAsync(applicationContext: Context?) {

}





