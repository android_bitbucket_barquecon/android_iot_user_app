package com.aowdriver.front_end_layer.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aowdriver.R;

import java.util.ArrayList;

public class InstallmentAdapter extends RecyclerView.Adapter<InstallmentAdapter.ViewHolder>{

    Context context;
    private ArrayList<String> arrayList;

    public InstallmentAdapter(Context activity, ArrayList<String> listVehicle) {
        this.context=activity;
        this.arrayList=listVehicle;

    }


    @Override
    public InstallmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the list_item_switch file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_installment, parent, false);
        InstallmentAdapter.ViewHolder gvh = new InstallmentAdapter.ViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(InstallmentAdapter.ViewHolder holder, final int position) {

        holder.txtview.setText(arrayList.get(position).toString());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtview;
        public ViewHolder(View view) {
            super(view);

            txtview=view.findViewById(R.id.txtMonth);
        }
    }
}