package com.aowdriver.front_end_layer.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aowdriver.R;
import com.aowdriver.front_end_layer.view.adapter.InstallmentAdapter;

import java.util.ArrayList;

public class FragmentLoan extends Fragment {

    private View myView;

    private RecyclerView mListInstallment;
    private ArrayList<String> listInstallment = new ArrayList<>();


    public FragmentLoan() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the list_item_switch for this fragment
        myView = inflater.inflate(R.layout.fragment_loan, container, false);
        initView(myView);

        return myView;

    }

    private void initView(View myView) {
        mListInstallment = myView.findViewById(R.id.listInstallment);

        LinearLayoutManager mListInstallmentMangaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mListInstallment.setLayoutManager(mListInstallmentMangaer);


        listInstallment.add("JAN  ₹200");
        listInstallment.add("FEB  ₹800");
        listInstallment.add("APR  ₹500");

        InstallmentAdapter installmentAdapter = new InstallmentAdapter(getActivity(), listInstallment);
        mListInstallment.setAdapter(installmentAdapter);
    }

}
