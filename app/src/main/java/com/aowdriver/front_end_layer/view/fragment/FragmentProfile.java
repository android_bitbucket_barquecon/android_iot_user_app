package com.aowdriver.front_end_layer.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aowdriver.R;

public class FragmentProfile  extends Fragment {

    private View myView;


    public FragmentProfile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the list_item_switch for this fragment
        myView = inflater.inflate(R.layout.fragment_profile, container, false);
//        initView(myView);
//        Toast.makeText(getActivity(), "OnCreate", Toast.LENGTH_SHORT).show();
        return myView;

    }

}
