package com.aowdriver.front_end_layer.view.widgets.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by monica on 12/15/2015.
 */
public class CustomEdittextRegular extends EditText {


    public CustomEdittextRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEdittextRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEdittextRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        if (!isInEditMode()) {
            //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "OstrichSansRounded-Medium.otf");
//            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Roboto-Thin.ttf");
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Raleway-Regular.ttf");
           // Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "sofiapro-light.otf");
            setTypeface(tf);
        }

    }
}
