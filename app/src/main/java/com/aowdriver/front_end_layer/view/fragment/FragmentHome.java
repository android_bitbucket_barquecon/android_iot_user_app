package com.aowdriver.front_end_layer.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aowdriver.R;
import com.aowdriver.front_end_layer.activity.WeatherActivity;
import com.aowdriver.front_end_layer.view.adapter.DevicesGridAdapter;
import com.aowdriver.front_end_layer.view.adapter.EarningAdapter;
import com.aowdriver.front_end_layer.view.adapter.InstallmentAdapter;
import com.aowdriver.front_end_layer.view.adapter.VehicleAdapter;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.aowdriver.wraper_layer.retrofit.Config.DEVICE_MOBILE_NO;

public class FragmentHome extends Fragment {

    private View myView;
    private RecyclerView mListVechicle, mListEarningMonth, mListInstallment;
    private ArrayList<String> listVehicle = new ArrayList<>();
    private ArrayList<String> listMonth = new ArrayList<>();
    private ArrayList<String> listInstallment = new ArrayList<>();
    private static TextView temptxt, humiditytxt, lighttxt, moisturetxt, phtxt;
    public static boolean isConnected;
    public static MqttAndroidClient client;
    private RecyclerView mGridViewDevices;
    private LinearLayout mLayWather;
    private JSONObject  jsonObject;

    public static  TextView mTxtPhFrag,mTxtTempFrag,mTxtHumFrag;

    private int img[] = {R.drawable.ic_live_tv_black_24dp, R.drawable.ic_ac_black_24dp, R.drawable.ic_router_black_24dp, R.drawable.ic_fan_24dp, R.drawable.ic_watch_black_24dp, R.drawable.ic_buld_24dp, R.drawable.ic_remove_red_eye_black_24dp, R.drawable.ic_music_player_24dp};
    private String name[] = {"Live tv", "Room Ac", "Wifi", "Fan", "Watch", "Bulb", "CCTV", "Music Player"};



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the list_item_switch for this fragment
        myView = inflater.inflate(R.layout.fragment_home, container, false);
        initView(myView);
        onClick();
        callMqtt();
//        Toast.makeText(getActivity(), "OnCreate", Toast.LENGTH_SHORT).show();
        return myView;

    }


    private void initView(View myView) {

        mListVechicle = myView.findViewById(R.id.listVehicle);
        mGridViewDevices = myView.findViewById(R.id.gridview);
        mGridViewDevices.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        mLayWather = myView.findViewById(R.id.layWeather);
        mListEarningMonth = myView.findViewById(R.id.listEarningMonth);
        mListInstallment = myView.findViewById(R.id.listInstallment);
        temptxt = myView.findViewById(R.id.temptxt);
        mTxtHumFrag = myView.findViewById(R.id.txtHumidityFrag);
        mTxtPhFrag = myView.findViewById(R.id.txtPhFrag);
        mTxtTempFrag = myView.findViewById(R.id.txtTempFrag);

        humiditytxt = myView.findViewById(R.id.humiditytxt);
        lighttxt = myView.findViewById(R.id.lighttxt);
        moisturetxt = myView.findViewById(R.id.moisturetxt);
        phtxt = myView.findViewById(R.id.phtxt);

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mListVechicle.setLayoutManager(horizontalLayoutManager);

        LinearLayoutManager mListEarningMonthManger = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mListEarningMonth.setLayoutManager(mListEarningMonthManger);


        LinearLayoutManager mListInstallmentMangaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mListInstallment.setLayoutManager(mListInstallmentMangaer);


        listVehicle.add("22%");
        listVehicle.add("88%");
        listVehicle.add("30%");
        listVehicle.add("40%");


        listMonth.add("JAN  ");
        listMonth.add("FEB  ");
        listMonth.add("APR  ");


        listInstallment.add("JAN  ₹200");
        listInstallment.add("FEB  ₹800");
        listInstallment.add("APR  ₹500");
//        listMonth.add("MAR  ₹300");


        VehicleAdapter vehicleAdapter = new VehicleAdapter(getActivity(), listVehicle);
        mListVechicle.setAdapter(vehicleAdapter);


        EarningAdapter earningAdapter = new EarningAdapter(getActivity(), listMonth);
        mListEarningMonth.setAdapter(earningAdapter);


        InstallmentAdapter installmentAdapter = new InstallmentAdapter(getActivity(), listInstallment);
        mListInstallment.setAdapter(installmentAdapter);


        DevicesGridAdapter devicesGridAdapter = new DevicesGridAdapter(getActivity(), name, img);
        mGridViewDevices.setAdapter(devicesGridAdapter);
    }

    private void onClick() {


        mLayWather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WeatherActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onDestroyView() {
        if (getView() != null) {
            ViewGroup parent = (ViewGroup) getView().getParent();
            parent.removeAllViews();
        }
        super.onDestroyView();
    }


    public void getValues(JSONObject jsonObject) throws JSONException {

      //  Toast.makeText(getActivity(), "HIIIIIIIIIIIIIIIIII", Toast.LENGTH_LONG).show();

//        val messageString: String = message.toString()
//        val json = JSONObject(messageString)

        if (jsonObject.has("temperature")) {
            String temp = (String) jsonObject.get("temperature");

            mTxtTempFrag.setText(""+temp+" °C");

        }

        if (jsonObject.has("humidity")) {
            String temp = (String) jsonObject.get("humidity");

            mTxtHumFrag.setText(""+temp+" %");

        }

        if (jsonObject.has("ph")) {
            String temp = (String) jsonObject.get("ph");

            mTxtPhFrag.setText(""+temp+"");

        }

        if (jsonObject.has("imea")) {
            String temp = (String) jsonObject.get("imea");

            // Subcribe
            // call register deivce API and  subcribe bqt/device_status/"imeaNo"

            //DEVICE_MOBILE_NO =userInputDialogEditText.getText().toString();
            Uri uri = Uri.parse("smsto:"+DEVICE_MOBILE_NO);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            String UserMobileNo = "8669072307";
            intent.putExtra("sms_body", "#ACKBQTGPS*");
            startActivity(intent);



            mTxtPhFrag.setText(""+temp+"");

        }


    }


    public  void callMqtt()
    {
        //MQTTConnect options : setting version to MQTT 3.1.1
        MqttConnectOptions options = new MqttConnectOptions();
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
        options.setUserName("bqt");
        options.setPassword("bqt@123".toCharArray());

        //Below code binds MainActivity to Paho Android Service via provided MqttAndroidClient
        // client interface
        //Todo : Check why it wasn't connecting to test.mosquitto.org. Isn't that a public broker.
        //Todo : .check why client.subscribe was throwing NullPointerException  even on doing subToken.waitForCompletion()  for Async                  connection estabishment. and why it worked on subscribing from within client.connect’s onSuccess(). SO
        String clientId = MqttClient.generateClientId();
        client = new MqttAndroidClient(getActivity(), "tcp://52.8.144.182:1883",
                clientId);


        try {
            IMqttToken token = client.connect(options);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    isConnected = true;
                    Log.d(TAG, "onSuccess");
                    Toast.makeText(getActivity().getApplicationContext(), "Connection successful", Toast.LENGTH_SHORT).show();
                    //Subscribing to a topic door/status on broker.hivemq.com
                    client.setCallback((MqttCallback) getContext());
                    final String topic = "bqt/sensorData"; //topic_at_which_you_want_app_to_subscribe
                     String UserMobileNo="8669072307";
                    String Topic1="bqt/getdevice_Imea/"+UserMobileNo;

                    String Topic2="bqt/device_status/867959033019388";

                    String Topic3="bqt/set_switch/5C:CF:7F:78:0E:DA";
                    int qos = 1;
                    try {
                        IMqttToken subToken = client.subscribe(topic, qos);

                         subToken = client.subscribe(Topic1, qos);
                        subToken = client.subscribe(Topic2, qos);
                        subToken = client.subscribe(Topic3, qos);


                        subToken.setActionCallback(new IMqttActionListener() {
                            @Override
                            public void onSuccess(IMqttToken asyncActionToken) {
                                // successfully subscribed
                                Toast.makeText(getActivity(), "Successfully subscribed to: " + topic, Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onFailure(IMqttToken asyncActionToken,
                                                  Throwable exception) {
                                // The subscription could not be performed, maybe the user was not
                                // authorized to subscribe on the specified topic e.g. using wildcards
                                Toast.makeText(getActivity(), "Couldn't subscribe to: " + topic, Toast.LENGTH_SHORT).show();

                            }
                        });
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Log.d(TAG, "onFailure");
                    Toast.makeText(getActivity(), "Connection failed", Toast.LENGTH_SHORT).show();

                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }

    }

}
