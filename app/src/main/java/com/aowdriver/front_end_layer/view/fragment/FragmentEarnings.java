package com.aowdriver.front_end_layer.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aowdriver.R;
import com.aowdriver.front_end_layer.view.adapter.EarningFragmentAdapter;

import java.util.ArrayList;

public class FragmentEarnings  extends Fragment {

    private View myView;
    private RecyclerView mListEarningMonth;

    private ArrayList<String> listMonth= new ArrayList<>();



    public FragmentEarnings() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the list_item_switch for this fragment
        myView = inflater.inflate(R.layout.fragment_earnings, container, false);
        initView(myView);
//        Toast.makeText(getActivity(), "OnCreate", Toast.LENGTH_SHORT).show();
        return myView;

    }

    private void initView(View myView)
    {


        mListEarningMonth=myView.findViewById(R.id.listEarningMonth);


        LinearLayoutManager mListEarningMonthManger = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mListEarningMonth.setLayoutManager(mListEarningMonthManger);


        listMonth.add("JAN  ");
        listMonth.add("FEB  ");
        listMonth.add("MAR  ");
        listMonth.add("APR  ");
        listMonth.add("MAY  ");
        listMonth.add("JUN  ");

        listMonth.add("JUL  ");
        listMonth.add("AUG  ");
        listMonth.add("SEP  ");
        listMonth.add("OCT  ");
        listMonth.add("NOV  ");
        listMonth.add("DEC  ");

        EarningFragmentAdapter earningAdapter = new EarningFragmentAdapter(getActivity(),listMonth);
        mListEarningMonth.setAdapter(earningAdapter);
    }


}
