package com.aowdriver.intigration_layer.model.request_model;

public class device_data_access_model {

    String macAddress;
    String value;
    String deviceNAme;
    String  type;
    String updatedOn;
    String status;
    String isActive;
    String deviceid;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDeviceNAme() {
        return deviceNAme;
    }

    public void setDeviceNAme(String deviceNAme) {
        this.deviceNAme = deviceNAme;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
