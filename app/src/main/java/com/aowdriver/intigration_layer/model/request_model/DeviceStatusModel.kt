package com.aowdriver.intigration_layer.model.request_model

import org.json.JSONObject

class DeviceStatusModel {
    
     var macAddress: String =""
     var value: String=""
     var deviceNAme: String=""
     var type: String=""
     var updatedOn: String=""
     var status: String=""
     var isActive: String=""
     var deviceid: String = ""
     lateinit var jsonSwitch: JSONObject


}