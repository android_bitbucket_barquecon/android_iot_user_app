package com.aowdriver.intigration_layer.uitlity

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.TextView
import com.aowdriver.R
import java.util.regex.Pattern

class Utility
{

internal val TAG = "Utility"
private var dialog: Dialog? = null
private val mMediaProcess: Dialog? = null
private val mProgressDialog: ProgressDialog? = null

//  dialog
open var dialogs: AlertDialog? = null
fun isConnectedToInternet(context: Context): Boolean {
    //mContext = context;
    val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    @SuppressLint("MissingPermission") val info = connectivity.allNetworkInfo
    if (info != null) {
        for (i in info.indices) {
            if (info[i].state == NetworkInfo.State.CONNECTED) {
                return true
            }
        }
    }
    return false
}


@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
fun Snackbar(view: View, msg: String) {




}


fun showProgressDialog(mContext: Context) {

    try {

        dialog = Dialog(mContext)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setContentView(R.layout.dialog_progress)
        // dialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        dialog!!.show()
        dialog!!.setCancelable(true)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}





fun dismissProgressDialog() {
    if (dialog != null && dialog!!.isShowing)
        dialog!!.dismiss()
}

companion object {
    private var mUtility: Utility? = null
    private val mContext: Context? = null


    val sharedInstance: Utility
        get() {
            if (mUtility == null) {
                mUtility = Utility()
            }
            return mUtility as Utility
        }

    fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    fun replaceWithPattern(str: String?, replace: String): String {
        if (str != null && str.length > 0) {
            val ptn = Pattern.compile("\\s+")
            val mtch = ptn.matcher(str)
            return mtch.replaceAll(replace)
        } else {
            return ""
        }
    }
}


}
