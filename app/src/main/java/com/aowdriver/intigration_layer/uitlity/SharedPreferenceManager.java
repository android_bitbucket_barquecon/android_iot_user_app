package com.aowdriver.intigration_layer.uitlity;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by VpaceTech on 1/2/2017.
 */

public class SharedPreferenceManager {


    private final SharedPreferences mPref;

    public static final String KEY_MED_TYPE_ID = "medTypeID";
    public static final String KEY_MED_TOOLNAME = "medTolname";
    public static final String KEY_MUSIC_TAB = "musicTab";
    public static final String KEY_INETR_TAB = "intrTab";
    public static final String KEY_AUDIO_TAB = "auidoTab";
    public static final String KEY_DISCOURSE_AUDIO = "discorseName";
    public static final String KEY_CAT_NAME = "cat_name";
    public static final String KEY_LANG_ID = "langID";
    public static final String KEY_CAT_ORIGINAL = "or_cat";
    public static final String KEY_LOAD_PLAY_PROGRSS = "progess";
    public static final String KEY_PLAY_POS = "0";
    public static final String KEY_MUSIC_MODE = "mode";
    public static final String KEY_CAT = "category";
    public static final String KEY_TYPE = "type";
    public static final String KEY_IS_OWN_COMMUNITY = "own_Com";
    public static final String KEY_COMMUNITY_NAME = "com_name";
    public static final String KEY_DRAWER_TYPE = "drawer_type";
    public static final String PREF_NAME = "com.music.av";
    public static final String KEY_PLAYLIST_ID = "play_list_id";
    public static final String X_API_KEY = "X-API-KEY";
    public static final String KEY_USER_ID = "id";
    public static final String KEY_MASTER_ID = "materId";
    public static final String KEY_USER_NAME = "username";
    public static final String KEY_IS_LOGIN = "00";
    public static final String KEY_Password = "password";
    public static final String KEY_USER_EMAIL = "email";
    public static final String KEY_ABOUT_ME = "about_me";
    public static final String KEY_PROFILE_PHOTO = "profile_photo";
    public static final String KEY_COMM_ID = "comm_id";
    public static final String KEY_IS_BANNED = "is_banned";
    public static final String KEY_STATUS = "status";
    public static final String KEY_TXT_MORE = "txt_more";
    public static final String KEY_COMM_PARENT_ID_COMMUNITY = "commParentID_COMM";
    public static final String KEY_TRACK_ID = "id_track";
    public static final String KEY_TRACK_TYPE = "track_type";
    public static final String KEY_FRAG_MAINTAIN = "maintaince";
    public static final String KEY_EVENT_ID = "event_id";

    public static final String KEY_MUSIC_MAINTAIN="mscMain";
    public static int CLICKEDIT = 1;

    private static SharedPreferenceManager sInstance;


    public SharedPreferenceManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPreferenceManager(context);
        }
    }


    public static synchronized SharedPreferenceManager getInstance() {
        if (sInstance == null) {
//            throw new IllegalStateException(SharedPreferenceManager.class.getSimpleName() +
//                    " is not initialized, call initializeInstance(..) method first.");

        }
        return sInstance;
    }

    // for boolean value
    public void writeBoolean(String key, boolean value) {
        mPref.edit().putBoolean(key, value).commit();
    }

    public boolean readBoolean(String key, boolean defValue) {
        return mPref.getBoolean(key, defValue);
    }

    // for integer value
    public void writeInteger(String key, int value) {
        mPref.edit().putInt(key, value).commit();

    }

    public int readInteger(String key, int defValue) {
        return mPref.getInt(key, defValue);
    }

    // for String value
    public String writeString(String key, String value) {
        mPref.edit().putString(key, value).commit();

        return key;
    }

    public String readString(String key) {

        return mPref.getString(key, null);
    }

    public String readString(String key, String defValue) {
        return mPref.getString(key, defValue);
    }

    // for float value
    public void writeFloat(String key, float value) {
        mPref.edit().putFloat(key, value).commit();
    }

    public float readFloat(String key, float defValue) {
        return mPref.getFloat(key, defValue);
    }

    // for long value
    public void writeLong(String key, long value) {
        mPref.edit().putLong(key, value).commit();
    }

    public long readLong(String key, long defValue) {
        return mPref.getLong(key, defValue);
    }

    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }
}


